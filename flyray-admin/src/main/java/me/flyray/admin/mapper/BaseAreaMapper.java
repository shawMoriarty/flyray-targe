package me.flyray.admin.mapper;

import me.flyray.admin.entity.BaseArea;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 行政区划
 * 
 * @author chj
 * @email ${email}
 * @date 2018-09-19 16:28:11
 */
@org.apache.ibatis.annotations.Mapper
public interface BaseAreaMapper extends Mapper<BaseArea> {
	
	List<BaseArea> listAreaByParentCode(Map<String, Object> param);
}
