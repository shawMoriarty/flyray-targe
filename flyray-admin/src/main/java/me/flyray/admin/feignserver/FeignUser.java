package me.flyray.admin.feignserver;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import me.flyray.auth.common.user.UserInfo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import me.flyray.admin.biz.UserBiz;
import me.flyray.admin.entity.User;
import me.flyray.common.msg.ResponseCode;
import me.flyray.common.util.EntityUtils;

@Controller
@RequestMapping("feign/user")
public class FeignUser {
	@Autowired
	private UserBiz userBiz;
	
	@RequestMapping(value = "add",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> addUser(@RequestBody Map<String, Object> param){
		Map<String, Object> result = new HashMap<String, Object>();
    	try {
    		result = userBiz.add(param);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("code", ResponseCode.SERVICE_NOT_AVALIABLE.getCode());
			result.put("message", ResponseCode.SERVICE_NOT_AVALIABLE.getMessage());
			result.put("success", false);
		}
    	return result;
    }

	@RequestMapping(value = "register",method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> register(@RequestBody Map<String, Object> param){
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			result = userBiz.register(param);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("code", ResponseCode.SERVICE_NOT_AVALIABLE.getCode());
			result.put("message", ResponseCode.SERVICE_NOT_AVALIABLE.getMessage());
			result.put("success", false);
		}
		return result;
	}

	/**
	 * 根据条件查询用户
	 */
	@RequestMapping(value = "query",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> query(@RequestBody Map<String, Object> param){
		Map<String, Object> result = new HashMap<>();
		User entity = EntityUtils.map2Bean(param, User.class);
		List<User> list = userBiz.selectList(entity);
		result.put("list", list);
		result.put("code", ResponseCode.OK.getCode());
		result.put("message", ResponseCode.OK.getMessage());
		return result;
	}
	
	@RequestMapping(value = "/queryByMobile", method = RequestMethod.POST)
	@ResponseBody
    public Map<String, Object> validate(@RequestBody Map<String,String> body){
		Map<String, Object> result = new HashMap<>();
    	String mobilePhone = body.get("mobilePhone");
    	User user = userBiz.getUserByMobilePhone(mobilePhone);
    	if (null != user) {
    		UserInfo info = new UserInfo();
	        BeanUtils.copyProperties(user, info);
	        info.setId(user.getId().toString());
	        result.put("info", info);
		}else{
			result.put("info", null);
		}
		result.put("code", ResponseCode.OK.getCode());
		result.put("message", ResponseCode.OK.getMessage());
    	return result;
    }
}
