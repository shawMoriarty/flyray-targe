package me.flyray.crm.core.feignserver;

import java.util.Map;

import javax.validation.Valid;

import me.flyray.common.msg.BaseApiResponse;
import me.flyray.crm.facade.request.AccountTransferRequest;
import me.flyray.crm.facade.request.UnfreezeAndTransferRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import me.flyray.crm.core.biz.customer.CustomerAccountTransferBiz;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/***
 * 转账相关的接口
 * */
@Api(tags="转账相关的接口")
@Controller
@RequestMapping("feign/customer")
public class FeignCustomerAccountTransfer {
	
	@Autowired
	private CustomerAccountTransferBiz customerAccountTransferBiz;


	/**
	 * @param 
	 * @return
	 */
	@ApiOperation("个人或者商户转账相关的接口")
	@RequestMapping(value = "/accounttransfer",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> accountTransfer(@RequestBody @Valid AccountTransferRequest accountTransferRequest){
		Map<String, Object> response = customerAccountTransferBiz.accountTransfer(accountTransferRequest);
		return response;
    }

	/***
	 * 个人或者商户解冻并转账接口
	 * 适用于提现
	 * */
	@ApiOperation("个人或者商户解冻并转账接口")
	@RequestMapping(value = "/unfreezeAndTransfer",method = RequestMethod.POST)
	@ResponseBody
	public BaseApiResponse<Void> unfreezeAndTransfer(@RequestBody @Valid UnfreezeAndTransferRequest unFreezeAndTransferRequest){
		customerAccountTransferBiz.unfreezeAndTransfer(unFreezeAndTransferRequest);
		return BaseApiResponse.newSuccess();
	}
	
}
