package me.flyray.crm.core.feignserver;

import me.flyray.common.msg.BaseApiResponse;
import me.flyray.crm.core.biz.customer.CustomerIntoAccountBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import me.flyray.crm.facade.request.IntoAccountRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.Valid;

/***
 * 入账相关的接口
 * */
@Api(tags="入账相关的接口")
@Controller
@RequestMapping("feign/customer")
public class FeignCustomerIntoAccount {
	
	@Autowired
	private CustomerIntoAccountBiz customerIntoAccountBiz;
	
	/**
	 * @param
	 * @des  根据交易类型增加总交易账户
	 * @return
	 */
	@ApiOperation("个人或者商户入账相关的接口")
	@RequestMapping(value = "/intoAccount",method = RequestMethod.POST)
    @ResponseBody
    public BaseApiResponse<Void> intoAccount(@RequestBody @Valid IntoAccountRequest intoAccountRequest){
		customerIntoAccountBiz.intoAccount(intoAccountRequest);
		return BaseApiResponse.newSuccess();
    }
	
}
