package me.flyray.crm.core.feignserver;

import javax.validation.Valid;

import me.flyray.common.msg.BaseApiResponse;
import me.flyray.crm.facade.request.OutAccountRequest;
import me.flyray.crm.facade.request.UnfreezeAndOutAccountRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import me.flyray.crm.core.biz.customer.CustomerOutAccountBiz;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/***
 * 出账相关的接口
 * */
@Api(tags="出账相关的接口")
@Controller
@RequestMapping("feign/customer")
public class FeignCustomerOutAccount {
	
	@Autowired
	private CustomerOutAccountBiz outAccountBiz;
	

	/**
	 * 个人或者商户直接出账的接口
	 * */
	@ApiOperation("个人或者商户直接出账的接口")
	@RequestMapping(value = "/outAccount",method = RequestMethod.POST)
    @ResponseBody
    public BaseApiResponse<Void> outAccount(@RequestBody @Valid OutAccountRequest outAccountRequest){
		outAccountBiz.outAccount(outAccountRequest);
		return BaseApiResponse.newSuccess();
    }

	/***
	 * 个人或者商户解冻并出账的接口
	 * */
	@ApiOperation("个人或者商户解冻并出账的接口")
	@RequestMapping(value = "/unfreezeAndOutAccount",method = RequestMethod.POST)
	@ResponseBody
	public BaseApiResponse<Void> unfreezeAndOutAccount(@RequestBody @Valid UnfreezeAndOutAccountRequest unFreezeAndOutAccountRequest){
		outAccountBiz.unfreezeAndOutAccount(unFreezeAndOutAccountRequest);
		return BaseApiResponse.newSuccess();
	}



}
