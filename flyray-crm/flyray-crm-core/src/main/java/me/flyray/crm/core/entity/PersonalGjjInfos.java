package me.flyray.crm.core.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 公积金账户信息
 * 
 * @author yyn
 *
 */
@Table(name = "personal_gjj_info")
public class PersonalGjjInfos {

	@Id
	@Column(name = "id")
	private String id;
	/**
	 * 客户会员号（个人客户编号）
	 */
	@Column(name = "personal_id")
	private String personalId;

	/**
	 * 平台编号
	 */
	@Column(name = "platform_id")
	private String platformId;

	/**
	 * 职工姓名
	 */
	@Column(name = "spname")
	private String spname;

	/**
	 * 性别
	 */
	@Column(name = "sex")
	private String sex;

	/**
	 * 身份证号码
	 */
	@Column(name = "spidno")
	private String spidNo;

	/**
	 * 账户状态
	 */
	@Column(name = "zhzt")
	private String zhzt;

	/**
	 * 手机号
	 */
	@Column(name = "sjh")
	private String sjh;

	/**
	 * 所属机构名称
	 */
	@Column(name = "orgname")
	private String orgName;

	/**
	 * 单位编码
	 */
	@Column(name = "sncode")
	private String snCode;

	/**
	 * 单位名称
	 */
	@Column(name = "snname")
	private String snName;

	/**
	 * 职工账号
	 */
	@Column(name = "spcode")
	private String spCode;

	/**
	 * 开户日期
	 */
	@Column(name = "khrq")
	private String khrq;

	/**
	 * 工资基数
	 */
	@Column(name = "spgz")
	private BigDecimal spgz;

	/**
	 * 个人缴存比例
	 */
	@Column(name = "spsingl")
	private BigDecimal spsingl;

	/**
	 * 单位缴存比例
	 */
	@Column(name = "spjcbl")
	private BigDecimal spjcbl;

	/**
	 * 月汇缴额
	 */
	@Column(name = "spmfact")
	private BigDecimal spmfact;

	/**
	 * 余额
	 */
	@Column(name = "spmend")
	private BigDecimal spmend;

	/**
	 * 汇缴年月
	 */
	@Column(name = "spjym")
	private String spjym;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPersonalId() {
		return personalId;
	}

	public void setPersonalId(String personalId) {
		this.personalId = personalId;
	}

	public String getPlatformId() {
		return platformId;
	}

	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}

	public String getSpname() {
		return spname;
	}

	public void setSpname(String spname) {
		this.spname = spname;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getSpidNo() {
		return spidNo;
	}

	public void setSpidNo(String spidNo) {
		this.spidNo = spidNo;
	}

	public String getZhzt() {
		return zhzt;
	}

	public void setZhzt(String zhzt) {
		this.zhzt = zhzt;
	}

	public String getSjh() {
		return sjh;
	}

	public void setSjh(String sjh) {
		this.sjh = sjh;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getSnCode() {
		return snCode;
	}

	public void setSnCode(String snCode) {
		this.snCode = snCode;
	}

	public String getSnName() {
		return snName;
	}

	public void setSnName(String snName) {
		this.snName = snName;
	}

	public String getSpCode() {
		return spCode;
	}

	public void setSpCode(String spCode) {
		this.spCode = spCode;
	}

	public String getKhrq() {
		return khrq;
	}

	public void setKhrq(String khrq) {
		this.khrq = khrq;
	}

	public BigDecimal getSpgz() {
		return spgz;
	}

	public void setSpgz(BigDecimal spgz) {
		this.spgz = spgz;
	}

	public BigDecimal getSpsingl() {
		return spsingl;
	}

	public void setSpsingl(BigDecimal spsingl) {
		this.spsingl = spsingl;
	}

	public BigDecimal getSpjcbl() {
		return spjcbl;
	}

	public void setSpjcbl(BigDecimal spjcbl) {
		this.spjcbl = spjcbl;
	}

	public BigDecimal getSpmfact() {
		return spmfact;
	}

	public void setSpmfact(BigDecimal spmfact) {
		this.spmfact = spmfact;
	}

	public BigDecimal getSpmend() {
		return spmend;
	}

	public void setSpmend(BigDecimal spmend) {
		this.spmend = spmend;
	}

	public String getSpjym() {
		return spjym;
	}

	public void setSpjym(String spjym) {
		this.spjym = spjym;
	}

	
}