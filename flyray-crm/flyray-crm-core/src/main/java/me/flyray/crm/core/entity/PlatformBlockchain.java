package me.flyray.crm.core.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * 
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-12-21 15:00:16
 */
@Table(name = "platform_blockchain")
public class PlatformBlockchain implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //平台编号
    @Id
    private String platformId;
	
	    //区块高度
    @Column(name = "block_hight")
    private Integer blockHight;
	
	    //生成时间
    @Column(name = "create_time")
    private Date createTime;
	
	    //上一个区块hash
    @Column(name = "previousHash")
    private String previoushash;
	
	    //区块hash
    @Column(name = "hash")
    private String hash;
	
	    //区块奖励数量
    @Column(name = "reward_amout")
    private BigDecimal rewardAmout;
	
	    //区块交易金额
    @Column(name = "amout")
    private BigDecimal amout;
	

	/**
	 * 设置：平台编号
	 */
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	/**
	 * 获取：平台编号
	 */
	public String getPlatformId() {
		return platformId;
	}
	/**
	 * 设置：区块高度
	 */
	public void setBlockHight(Integer blockHight) {
		this.blockHight = blockHight;
	}
	/**
	 * 获取：区块高度
	 */
	public Integer getBlockHight() {
		return blockHight;
	}
	/**
	 * 设置：生成时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：生成时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：上一个区块hash
	 */
	public void setPrevioushash(String previoushash) {
		this.previoushash = previoushash;
	}
	/**
	 * 获取：上一个区块hash
	 */
	public String getPrevioushash() {
		return previoushash;
	}
	/**
	 * 设置：区块hash
	 */
	public void setHash(String hash) {
		this.hash = hash;
	}
	/**
	 * 获取：区块hash
	 */
	public String getHash() {
		return hash;
	}
	/**
	 * 设置：区块奖励数量
	 */
	public void setRewardAmout(BigDecimal rewardAmout) {
		this.rewardAmout = rewardAmout;
	}
	/**
	 * 获取：区块奖励数量
	 */
	public BigDecimal getRewardAmout() {
		return rewardAmout;
	}
	/**
	 * 设置：区块交易金额
	 */
	public void setAmout(BigDecimal amout) {
		this.amout = amout;
	}
	/**
	 * 获取：区块交易金额
	 */
	public BigDecimal getAmout() {
		return amout;
	}
}
