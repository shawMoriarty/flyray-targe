package me.flyray.crm.core.feignserver;

import javax.validation.Valid;

import me.flyray.common.msg.BaseApiResponse;
import me.flyray.crm.core.entity.FreezeJournal;
import me.flyray.crm.core.entity.UnfreezeJournal;
import me.flyray.crm.facade.request.FreezeRequest;
import me.flyray.crm.facade.request.UnfreezeRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import me.flyray.crm.core.biz.customer.CustomerFreezeOrUnfreezeBiz;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/***
 * 冻结、解冻相关的接口
 * */
@Api(tags="冻结、解冻相关的接口")
@Controller
@RequestMapping("feign/customer")
public class FeignCustomerFreezeOrUnfreeze {
	
	@Autowired
	private CustomerFreezeOrUnfreezeBiz commonFreezefnFreezeBiz;
	
	
	/***
	 * 个人或者商户冻结
	 * */
	@ApiOperation("冻结接口")
	@RequestMapping(value = "/freeze",method = RequestMethod.POST)
    @ResponseBody
    public BaseApiResponse<FreezeJournal> freeze(@RequestBody @Valid FreezeRequest freezeRequest){
		FreezeJournal freezeJournal = commonFreezefnFreezeBiz.freeze(freezeRequest);
		return BaseApiResponse.newSuccess(freezeJournal);
	}
	
	/**
	 * 个人或者商户解冻
	 * */
	@ApiOperation("解冻接口")
	@RequestMapping(value = "/unfreeze",method = RequestMethod.POST)
    @ResponseBody
    public BaseApiResponse<UnfreezeJournal> unfreeze(@RequestBody @Valid UnfreezeRequest unFreezeRequest){
		UnfreezeJournal unfreezeJournal =  commonFreezefnFreezeBiz.unfreeze(unFreezeRequest);
		return BaseApiResponse.newSuccess(unfreezeJournal);
    }

}
