package me.flyray.crm.core.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Table;

import me.flyray.common.entity.BaseEntity;


/**
 * 冻结流水表
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-07-16 10:15:49
 */
@Table(name = "freeze_journal")
public class FreezeJournal extends BaseEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //流水号
	@Column(name = "journal_id")
    private String journalId;
	
	    //平台编号
    @Column(name = "platform_id")
    private String platformId;
	
	    //客户类型    CUST00：平台   CUST01：商户  CUST02：用户
    @Column(name = "customer_type")
    private String customerType;
	
	    //个人或商户编号
    @Column(name = "customer_no")
    private String customerNo;
	
	    //交易类型  01：充值，02：提现，
    @Column(name = "trade_type")
    private String tradeType;
	
	    //订单号
    @Column(name = "order_no")
    private String orderNo;
	
	    //冻结金额
    @Column(name = "freeze_balance")
    private BigDecimal freezeBalance;
	
	    //冻结状态 1：已冻结  2：部分解冻  3：已解冻
    @Column(name = "status")
    private String status;
	
	    //创建时间
    @Column(name = "create_time")
    private Date createTime;
	
	    //更新时间
    @Column(name = "update_time")
    private Date updateTime;

    //账户类型
	@Column(name = "account_type")
	private String accountType;

	/**
	 * 设置：流水号
	 */
	public void setJournalId(String journalId) {
		this.journalId = journalId;
	}
	/**
	 * 获取：流水号
	 */
	public String getJournalId() {
		return journalId;
	}
	/**
	 * 设置：平台编号
	 */
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	/**
	 * 获取：平台编号
	 */
	public String getPlatformId() {
		return platformId;
	}
	/**
	 * 设置：客户类型    CUST00：平台   CUST01：商户  CUST02：用户
	 */
	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}
	/**
	 * 获取：客户类型    CUST00：平台   CUST01：商户  CUST02：用户
	 */
	public String getCustomerType() {
		return customerType;
	}
	/**
	 * 设置：个人或商户编号
	 */
	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}
	/**
	 * 获取：个人或商户编号
	 */
	public String getCustomerNo() {
		return customerNo;
	}
	/**
	 * 设置：交易类型  01：充值，02：提现，
	 */
	public void setTradeType(String tradeType) {
		this.tradeType = tradeType;
	}
	/**
	 * 获取：交易类型  01：充值，02：提现，
	 */
	public String getTradeType() {
		return tradeType;
	}
	/**
	 * 设置：订单号
	 */
	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
	/**
	 * 获取：订单号
	 */
	public String getOrderNo() {
		return orderNo;
	}
	/**
	 * 设置：冻结金额
	 */
	public void setFreezeBalance(BigDecimal freezeBalance) {
		this.freezeBalance = freezeBalance;
	}
	/**
	 * 获取：冻结金额
	 */
	public BigDecimal getFreezeBalance() {
		return freezeBalance;
	}
	/**
	 * 设置：冻结状态 1：已冻结  2：部分解冻  3：已解冻
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * 获取：冻结状态 1：已冻结  2：部分解冻  3：已解冻
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：更新时间
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	/**
	 * 获取：更新时间
	 */
	public Date getUpdateTime() {
		return updateTime;
	}
	public String getAccountType() {
		return accountType;
	}
	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}
	
}
