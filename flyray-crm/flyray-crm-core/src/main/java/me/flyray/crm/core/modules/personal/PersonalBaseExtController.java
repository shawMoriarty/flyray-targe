package me.flyray.crm.core.modules.personal;

import me.flyray.common.rest.BaseController;
import me.flyray.crm.core.biz.personal.PersonalBaseExtBiz;
import me.flyray.crm.core.entity.PersonalBaseExt;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("personalBaseExt")
public class PersonalBaseExtController extends BaseController<PersonalBaseExtBiz, PersonalBaseExt> {

}