package me.flyray.crm.facade.response;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @Author: bolei
 * @date: 15:10 2019/1/11
 * @Description: 类描述
 */

@Data
public class UnfreezeJournalResponse implements Serializable {

    private String journalId;

    //冻结流水号
    private String freezeId;

    //订单号
    private String orderNo;

    //交易类型  01：充值，02：提现，
    private String tradeType;

    //解冻金额
    private BigDecimal unfreezeBalance;

    //创建时间
    private Date createTime;

}
