package me.flyray.crm.facade.response;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @Author: bolei
 * @date: 15:18 2019/1/11
 * @Description: 客户账户信息
 */

@Data
public class CustomerAccountQueryResponse implements Serializable {

    private String accountId;

    private String accountName;

    //平台编号
    private String platformId;

    //个人信息编号
    private String personalId;

    //
    private String customerId;

    //商户编号
    private String merchantId;

    //商户类型    CUST00：平台   CUST01：商户  CUST02：用户
    private String merchantType;

    //账户类型  ACC001：余额账户，ACC002：红包账户，ACC003：积分账户，ACC004：手续费账户，ACC005：已结算账户，ACC006：交易金额账户，ACC007：冻结金额账户，ACC008：平台管理费账户，ACC009：平台服务费账户，ACC010：火源账户，ACC011：火钻账户，ACC012：原力值账户，ACC013：可提现余额账户等......
    private String accountType;

    //币种  CNY：人民币
    private String ccy;

    //账户余额
    private BigDecimal accountBalance;

    //冻结金额
    private BigDecimal freezeBalance;

    //校验码（余额加密值）
    private String checkSum;

    //账户状态 00：正常，01：冻结
    private String status;

    //创建时间
    private Date createTime;

    //更新时间
    private Date updateTime;

}
