package me.flyray.common.enums;

/** 
* 出入账标识
*/

public enum InOutFlag {

	in("1","入账"),
	out("2","出账");
	
    private String code;
    private String desc;
    
    private InOutFlag(String code, String desc){
        this.code = code;
        this.desc = desc;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
    
    public static InOutFlag getCommentModuleNo(String code){
        for(InOutFlag o : InOutFlag.values()){
            if(o.getCode().equals(code)){
                return o;
            }
        }
        return null;
    }
}
